package com.xzbd.mail.utils.mailContentUtil;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

public class TextEmailContentItem extends AbstractEmailContentItem {
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    @Override
    public BodyPart getMailBodyPart() throws MessagingException {
        BodyPart bodyPart = new MimeBodyPart();
        bodyPart.setText(this.getContent());
        return bodyPart;
    }
}
